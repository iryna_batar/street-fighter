import {showModal} from "./modal";
import {createFighterImage} from "../fighterPreview";
import {createElement} from "../../helpers/domHelper";

export function showWinnerModal(fighter) {

    const IMAGE = createFighterImage(fighter);
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root right`,
    });
    fighterElement.append(IMAGE);


    showModal({title: `Winner: ${fighter.name}`, bodyElement: fighterElement, onClose: function () {
            window.location.reload()
        }});
}
