import {controls} from '../../constants/controls';
import {showWinnerModal} from "./modal/winner";
import FightAction from "./FightAction";

export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        const PLAYER_ONE = new FightAction(firstFighter, {KeyQ: false, KeyW: false, KeyE: false});
        const PLAYER_TWO = new FightAction(secondFighter, {KeyU: false, KeyI: false, KeyO: false});
        const HEALTH_BAR_PLAYER_ONE = document.getElementById('left-fighter-indicator');
        const HEALTH_BAR_PLAYER_TWO = document.getElementById('right-fighter-indicator');

        document.addEventListener('keydown', (key) => {

            if (key.code === controls.PlayerOneAttack && PLAYER_ONE.getBlockStatus()) {
                PLAYER_TWO.setDamage(getDamage(PLAYER_ONE.getFighter(), PLAYER_TWO.getFighter()));
                HEALTH_BAR_PLAYER_TWO.style.width = `${PLAYER_TWO.getHealthBarWidth()}%`;
            }

            if (key.code === controls.PlayerTwoAttack && PLAYER_TWO.getBlockStatus()) {
                PLAYER_ONE.setDamage(getDamage(PLAYER_TWO.getFighter(), PLAYER_ONE.getFighter()));
                HEALTH_BAR_PLAYER_ONE.style.width = `${PLAYER_ONE.getHealthBarWidth()}%`;
            }

            if (key.code === controls.PlayerOneBlock) {
                PLAYER_ONE.setBlockStatus(false);
            }

            if (key.code === controls.PlayerTwoBlock) {
                PLAYER_TWO.setBlockStatus(false);
            }

            if (controls.PlayerOneCriticalHitCombination.some(val => val === key.code) &&
                PLAYER_ONE.getBlockStatus()) {
                PLAYER_ONE.setCriticalHit(key.code);


                if (PLAYER_ONE.getCriticalHit()) {
                    PLAYER_TWO.setDamage(getCriticalDamage(PLAYER_ONE));
                    HEALTH_BAR_PLAYER_TWO.style.width = `${PLAYER_TWO.getHealthBarWidth(true)}%`;

                }
            }

            if (controls.PlayerTwoCriticalHitCombination.some(val => val === key.code) &&
                PLAYER_TWO.getBlockStatus()) {
                PLAYER_TWO.setCriticalHit(key.code);

                if (PLAYER_TWO.getCriticalHit()) {
                    PLAYER_ONE.setDamage(getCriticalDamage(PLAYER_TWO));
                    HEALTH_BAR_PLAYER_ONE.style.width = `${PLAYER_ONE.getHealthBarWidth(true)}%`;
                }
            }

            if (PLAYER_ONE.getFighter().health <= 0 || PLAYER_TWO.getFighter().health <= 0) {
                const WINNER = PLAYER_ONE.getFighter().health <= 0 ?
                    PLAYER_TWO.getFighter() : PLAYER_ONE.getFighter();

                resolve(WINNER);
            }
        });

        document.addEventListener('keyup', (key) => {

            if (key.code === controls.PlayerOneBlock) {
                PLAYER_ONE.setBlockStatus(true);
            }

            if(key.code === controls.PlayerTwoBlock) {
                PLAYER_TWO.setBlockStatus(true);
            }

            if (controls.PlayerOneCriticalHitCombination.some(val => val === key.code)) {
                PLAYER_ONE.setCriticalHit(key.code, false);
            }

            if (controls.PlayerTwoCriticalHitCombination.some(val => val === key.code)) {
                PLAYER_TWO.setCriticalHit(key.code, false);
            }
        });
    });
}

export function getDamage(attacker, defender) {
    let hitPowerAttacker = getHitPower(attacker);
    let blockPowerDefender = getBlockPower(defender);
    let damage = hitPowerAttacker - blockPowerDefender;
    return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
    let criticalHitChance = Math.random() + 1;
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    let dodgeChance = Math.random() + 1;
    return fighter.defense * dodgeChance;
}

function getCriticalDamage(attacker) {

    return 2 * attacker.getFighter().attack;
}