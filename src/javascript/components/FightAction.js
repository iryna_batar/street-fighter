
export default class FightAction {

    constructor(fighter, criticalHitCombination) {
        this.fighter = fighter;
        this.currentHealthBar = 100;
        this.healthBarPercent = 100 / fighter.health;
        this.damage = 0;
        this.activeCriticalHit = true;
        this.block = true;
        this.criticalHitCombination = criticalHitCombination;
    }

    getFighter (){

        return this.fighter;
    }

    setDamage (damage) {
        this.damage = damage;
    }

    getHealthBarWidth(criticalHit){
        if(this.block || criticalHit){
            this.currentHealthBar = this.currentHealthBar - (this.healthBarPercent * this.damage);
            this.fighter.health = this.fighter.health - this.damage;
        }

        return this.currentHealthBar;
    }

    setBlockStatus(status){
        this.block = status;
    }

    getBlockStatus() {

        return this.block;
}

    setCriticalHit (key) {
        this.criticalHitCombination[key] = this.criticalHitCombination[key] !== true;
    }

    getCriticalHit () {
        if(Object.values(this.criticalHitCombination).every(key => key === true) && this.activeCriticalHit) {
            this.activeCriticalHit = false;
            setTimeout(()=>{
                this.activeCriticalHit = true;
            }, 10000);

            return true;
        }
    }
}