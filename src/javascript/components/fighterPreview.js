import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const imgElement = createFighterImage(fighter);
  fighterElement.innerHTML = `<h3 class="fighter-preview___header">Name: ${fighter.name}</h3>
                                <p class="fighter-preview___info">Health: ${fighter.health}</p>
                                <p class="fighter-preview___info">Attack: ${fighter.attack}</p>
                                <p class="fighter-preview___info">Defense: ${fighter.defense}</p>`;

  fighterElement.append(imgElement);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
